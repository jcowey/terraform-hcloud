    resource "hcloud_server" "manager" {
      count = 3
      name = "manager0${count.index}.exnet.systems"
      image = "fedora-27"
      location = "nbg1"
      server_type = "cx11-ceph"
      ssh_keys = ["${hcloud_ssh_key.default.id}"]
    }

    resource "hcloud_server" "worker" {
      count = 4
      name = "node0${count.index}.exnet.systems"
      image = "fedora-27"
      location = "nbg1"
      server_type = "cx21-ceph"
      ssh_keys = ["${hcloud_ssh_key.default.id}"]
    }

    resource "hcloud_server" "ceph" {
      count = 3
      name = "ceph0${count.index}.exnet.systems"
      image = "fedora-27"
      location = "nbg1"
      server_type = "cx11-ceph"
      ssh_keys = ["${hcloud_ssh_key.default.id}"]
    }

    resource "hcloud_volume" "ceph" {
      count = 3
      name = "OSD-${count.index}"
      size = 32
      server_id = "${hcloud_server.ceph.id}"
    }

    resource "hcloud_ssh_key" "default" {
      name = "Terraform-WORK"
      public_key = "${file("~/.ssh/id_rsa.pub")}"
    }
