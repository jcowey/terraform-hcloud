# Hetzner Cloud 

---

## Terraform

Create a `terraform.tfvars` file with your API key

```
hcloud_token = "YOUR PROJECTS API KEY HERE"
```

Check out the tf file for what it creates
