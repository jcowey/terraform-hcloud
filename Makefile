SHELL := bash

deploy:
	terraform apply -auto-approve && HCLOUD_TOKEN=$(cat HCLOUD_VAR) ansible-playbook roles/swarm/swarm.yml -i hcloud.py -e ansible_user=root -e swarm_iface=ens192
destroy:
	terraform destroy
